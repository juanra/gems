const path = require( 'path' );
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {

    entry: './src/index.ts',

    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: 'main.js',
    },

    resolve: {
        extensions: ['.tsx', '.ts', '.js' ],
    },

    module: {
        rules: [
            {
                test: /\.tsx?/,
                use: 'ts-loader',
                exclude: /node_modules/,
                include: [
                    path.resolve(__dirname, "src")
                ],
            }
        ]
    },
    
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: "./assets" , to: "assets" },
                { from: "./src/index.html", to: "index.html"}
            ]}
        )
    ]
    
};