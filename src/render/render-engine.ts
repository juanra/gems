
import * as $ from "jquery";
import { WebGLRenderer, Camera, Scene, Raycaster, OrthographicCamera, Vector3, AmbientLight, DirectionalLight, Renderer, Vector2 } from "three";
import { DomainEngine } from "../domain/domain-engine";
import { ItemRender } from "./item/item-render";
import { ItemRenderCollection } from "./item/item-render-collection";
import { ItemRenderFactory } from "./item/item-render-factory";

export class RenderEngine {

    protected container = document.body;
    protected width = window.innerWidth;
    protected height = window.innerHeight;
    

    protected renderer: WebGLRenderer;
    protected camera: Camera;
    protected scene: Scene;
    protected itemFactory: ItemRenderFactory;

    protected collection: ItemRenderCollection;
    protected raycaster: Raycaster;

    protected uiTimer = 0;

    constructor(
        protected domain: DomainEngine) {

        this.initRenderer();
        this.initScene();
        this.initCamera();
        this.initLights();

        this.collection = new ItemRenderCollection(this.scene);
        this.itemFactory = new ItemRenderFactory();
        this.raycaster = new Raycaster();
    }

    protected initRenderer(): void {
        this.renderer = new WebGLRenderer({antialias: true});
        this.renderer.setSize( this.width, this.height);
        this.renderer.setClearColor( 0xcccccc, 1);
        this.renderer.shadowMap.enabled = true;
        this.container.appendChild(this.renderer.domElement);
    }

    protected initCamera(): void {

        const aspect = this.width / this.height;
        let x = 15;
        let y = x;
        if (aspect > 0){
            y = y / aspect;
            if (y < x){
                y = x;
                x = x * aspect;
            }
        }        
        this.camera = new OrthographicCamera(-x, x, y, -y, 1, 20);

        this.camera.position.set(10, 10, 2);
        this.camera.lookAt(new Vector3(10, 10, 0));
    }

    protected initScene(): void {
        this.scene = new Scene();
    }

    protected initLights(): void {
        const ambient = new AmbientLight(0xFFFFFF, 0.2);
        this.scene.add(ambient);

        const directional = new DirectionalLight();
        directional.position.set(7, 7, 10);
        this.scene.add(directional);
    }

    getRenderer(): Renderer{
        return this.renderer;
    }

    getCollection(): ItemRenderCollection {
        return this.collection;
    }

    getItemRenderFactory(): ItemRenderFactory {
        return this.itemFactory;
    }

    init(): void {

        window.addEventListener('resize', () => {
            this.width = window.innerWidth;
            this.height = window.innerHeight;
            this.renderer.setSize(this.width, this.height);
            this.initCamera();
        }, false);
    }

    update(dt: number): void {
        const deleted = this.collection.filter((item) => item.isDeleted());
        this.collection.removeAll(deleted);

        this.collection.map((item) => item.update(dt));

        this.updateUI(dt);

        this.renderer.render(this.scene, this.camera);
    }

    pick(mouse: Vector2): ItemRender {
        this.raycaster.setFromCamera(mouse, this.camera);
        let pick: ItemRender  = null;
        this.collection.map((i) => {
            const intersect = this.raycaster.intersectObject(i.getObject(), true);
            if(intersect.length > 0) {
                pick = i;
                return;
            }
        });  
        return pick;      
    }  
    
    updateUI(dt: number): void {
        this.uiTimer += dt;
        if (this.uiTimer < 1){
            return;
        }
        $("#points .value").html(this.domain.getPoints().get().toString());
        $("#moves .value").html(this.domain.getMoves().get().toString());
        this.uiTimer = 0;
    }
}