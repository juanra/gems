import { Object3D } from "three";
import { Cell } from "../../domain/cell/cell";
import { ItemRender } from "./item-render";
import { RenderAnimation } from "./render-animation";


export class FallItemRenderAnimation extends RenderAnimation {

    protected delayTimer = 0;
    protected timer = 0;
    protected object: Object3D;
    protected trail: Cell[] = [];

    constructor(protected itemRender: ItemRender, delay: number, protected delaySteps: number){
        super(delay);
        this.object = this.itemRender.getObject();
        this.trail = Object.assign([], this.itemRender.getItem().getTrail());   
    }

    update(dt: number): void {
        this.delayTimer += dt;
        if (this.delayTimer < this.delay){
            return;
        }
        
        this.timer += dt;
        if (this.timer < this.delaySteps){
            return;
        }

        const target = this.trail.shift();
        if(!target){
            this.done = true;
            this.itemRender.getItem().clearTrail();
            return;
        }    
        this.object.position.set(target.col * 2.5, target.row * 2.5, 0); 
        this.timer = 0;        
    }
}