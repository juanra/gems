import { Scene } from "three";
import { ItemRender } from "./item-render";

export class ItemRenderCollection {

    protected collection: ItemRender[] = []

    constructor(protected scene: Scene) {}

    find(id: string): ItemRender {
        return this.collection.find((i) => i.getId() == id);
    }

    map(fn: (item: ItemRender) => any): ItemRenderCollection {
        this.collection.map(fn);
        return this;
    }

    filter(fn: (item: ItemRender) => any): ItemRender[] {
        return this.collection.filter(fn);        
    }

    add(item: ItemRender): void {
        this.collection.push(item);
        this.scene.add(item.getObject());
    }

    remove(item: ItemRender): void {
        const index = this.collection.findIndex((i) => i.getId() == item.getId());
        if (index >= 0) {
            this.collection.splice(index, 1);
            this.scene.remove(item.getObject());            
        }
    }

    removeAll(items: ItemRender[]): void {
        items.map((item) => this.remove(item));
    }
}