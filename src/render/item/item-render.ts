import { Object3D } from "three";
import { Item } from "../../domain/item/item";
import { RenderAnimation } from "./render-animation";


export class ItemRender {

    protected new = true;
    protected deleted = false;
    protected animations: RenderAnimation[] = [];

    constructor(protected item: Item, protected object: Object3D) {}

    addAnimation(animation: RenderAnimation): void {
        this.animations.push(animation);
    }

    getId(): string {
        return this.item.getId();
    }

    getItem(): Item {
        return this.item;
    }

    getObject(): Object3D {
        return this.object;
    }

    update(dt: number): void {
        this.animations.map((animation) => animation.update(dt));
        this.animations = this.animations.filter((animation) => !animation.isDone());
    }

    isNew(): boolean{
        return this.new;
    }

    setNew(value: boolean): void {
        this.new = value;
    }
   
    isDeleted(): boolean{
        return this.deleted;
    }

    setDeleted(value: boolean): void {
        this.deleted = value;    
    }
}