export class RenderAnimation {

    protected done = false;

    constructor(protected delay = 0) {}

    isDone(): boolean {
        return this.done;
    }

    update(dt: number): void {

    }
}