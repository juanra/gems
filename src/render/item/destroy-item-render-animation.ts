import { ItemRender } from "./item-render";
import { RenderAnimation } from "./render-animation";


export class DestroyItemRenderAnimation extends RenderAnimation {

    protected delayTimer = 0;
    protected scale = 1;

    constructor(protected itemRender: ItemRender, delay: number){
        super(delay);
    }

    update(dt: number): void {
        this.delayTimer += dt;
        if (this.delayTimer < this.delay){
            return;
        }
        const object = this.itemRender.getObject();

        this.scale -= dt * 2;
        
        if (this.scale < 0) {
            this.scale = 0;
            this.done = true;
            object.visible = false;        
        }

        object.scale.set(this.scale, this.scale, this.scale);          
    }
}