import { Mesh, IcosahedronGeometry, SphereGeometry, MeshPhongMaterial } from "three";
import { Item } from "../../domain/item/item";
import { ItemRender } from "./item-render";


export class ItemRenderFactory {

    makeItemRender(item: Item): ItemRender{
        const type = item.getType();
        const level = item.getLevel();
        const geometry = this.getGeometry(type, level);
        const material = this.getMaterial(type, level);
        const object = new Mesh(geometry, material);
        return new ItemRender(item, object);
    }

    protected getGeometry(type: number, level: number): any {
        let geometry = null;
        switch(level){
            case 2:
                geometry = new IcosahedronGeometry(1);
                break;
            default:
                geometry = new SphereGeometry(1, 30, 30);
                break;
        }   
        return geometry;     
    }

    protected getMaterial(type: number, level: number): any {
        return new MeshPhongMaterial({color: this.getColorByType(type)});
    }

    protected getColorByType(type: number): any {
        switch(type){
            case 1:
                return 0xFF0000;
            case 2:
                return 0x00FF00;
            case 3: 
                return 0x0000FF;
            case 4:
                return 0xFFFF00;
            case 5:
                return 0xFF00FF;
        }
    }
}