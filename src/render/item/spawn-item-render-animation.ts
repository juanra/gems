import { Object3D } from "three";
import { ItemRender } from "./item-render";
import { RenderAnimation } from "./render-animation";

export class SpawnItemRenderAnimation extends RenderAnimation {

    protected object: Object3D;
    protected scale = 0;
    protected delayTimer = 0;

    constructor(protected itemRender: ItemRender, delay: number){
        super(delay);
        this.object = this.itemRender.getObject();
        const cell = this.itemRender.getItem().getTrail()[0];         
        this.object.scale.multiplyScalar(0);    
        this.object.position.set(cell.col * 2.5, cell.row * 2.5, 0);  
        
    }

    update(dt: number): void {
        this.delayTimer += dt;
        if (this.delayTimer < this.delay){
            return;
        }
        this.scale = 1;
        this.done = true;
        this.itemRender.setNew(false);
        this.object.scale.set(this.scale, this.scale, this.scale);
                 
    }
}