import { Item } from "../item/item";

export class Cell {

    protected item: Item = null;
    
    constructor(
        public readonly row: number,
        public readonly col: number) {}

    setItem(item: Item): void {
        this.item = item;
    }

    getItem(): Item {
        return this.item;
    }
}