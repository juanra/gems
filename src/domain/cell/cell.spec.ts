import { Item } from "../item/item";
import { Cell } from "./cell";


describe("Cell", ()=> {

    let cell: Cell;

    beforeEach(() => {
        cell = new Cell(3, 9);
    });

    it("row value", () => {
        expect(cell.row).toBe(3);
    });

    it("col value", () => {
        expect(cell.col).toBe(9);
    });

    it("nullable item", () => {
        expect(cell.getItem()).toBeNull();
    });

    it("set item", () => {
        const item = new Item(3);
        cell.setItem(item);
        expect(cell.getItem()).toBe(item);
    });
});