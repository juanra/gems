import { CellMatrix } from "../cell-matrix/cell-matrix";

export class ItemSwapper {
    constructor() {}

    swap(matrix: CellMatrix, rowA: number, colA: number, rowB: number, colB: number): void {
        const a = matrix.get(rowA, colA);
        const b = matrix.get(rowB, colB);
        matrix.set(rowA, colA, b);
        matrix.set(rowB, colB, a);
    }
}