import { CellMatrix } from "../cell-matrix/cell-matrix";
import { Item } from "./item";
import { ItemMatcher } from "./item-matcher";


describe("ItemMatcher", () => {

    let matcher: ItemMatcher;
    let matrix: CellMatrix;

    beforeAll(() => {
        matcher = new ItemMatcher();
    });

    beforeEach(() => {
        matrix = new CellMatrix(9 , 9);
    });

    it("No matches on empty matrix", () => {
        const matches = matcher.findMatches([]);
        expect(matches.length).toBe(0);
    });

    it("No matches on row without items", () => {        
        const cells = matrix.getRow(0);
        const matches = matcher.findMatches(cells);
        expect(matches.length).toBe(0);
    });

    it("find exactly 3 matches", () => {     
        matrix.set(0, 0, new Item(1));
        matrix.set(0, 1, new Item(1));
        matrix.set(0, 2, new Item(1));   
        const cells = matrix.getRow(0);
        const matches = matcher.findMatches(cells);
        expect(matches.length).toBe(3);
    });

    it("find 0 matches on 3 different items", () => {     
        matrix.set(0, 0, new Item(1));
        matrix.set(0, 1, new Item(2));
        matrix.set(0, 2, new Item(3));   
        const cells = matrix.getRow(0);
        const matches = matcher.findMatches(cells);
        expect(matches.length).toBe(0);
    });

    it("find more than 3 matches with null between", () => {     
        matrix.set(0, 0, new Item(1));
        matrix.set(0, 1, null);
        matrix.set(0, 2, new Item(1));
        matrix.set(0, 3, new Item(2));
        matrix.set(0, 4, new Item(1));
        matrix.set(0, 5, new Item(1));
        matrix.set(0, 6, new Item(1)); 
        matrix.set(0, 7, new Item(1)); 
        matrix.set(0, 8, new Item(3)); 
        const cells = matrix.getRow(0);
        const matches = matcher.findMatches(cells);
        expect(matches.length).toBe(4);
    });

    it("find matches in last element", () => {     
        matrix.set(0, 6, new Item(1)); 
        matrix.set(0, 7, new Item(1)); 
        matrix.set(0, 8, new Item(1)); 
        const cells = matrix.getRow(0);
        const matches = matcher.findMatches(cells);
        expect(matches.length).toBe(3);
    });
});