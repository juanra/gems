import { Item } from "./item";
import { ItemFactory } from "./item-factory";

describe("ItemFactory", ()=> {

    let factory: ItemFactory;

    beforeAll(() => {
        factory = new ItemFactory();
    })

    it("make item", () => {
        const item = factory.makeItem(3);
        expect(item.getType()).toBe(3);
    })

    it("make random item", () => {
        const item = factory.makeRandomItem();
        expect(item instanceof Item).toBeTruthy();
    })
})