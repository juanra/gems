import { Cell } from "../cell/cell";
import { Item } from "./item";

describe("Item", () => {

    it("new", () => {
        const item = new Item(3);
        expect(item.getType()).toBe(3);
    });

    it("track cell", () => {
        const item = new Item(3);
        const cell = new Cell(0, 0);
        item.addCell(cell);
        expect(item.getCurrentCell()).not.toBeNull();
        expect(item.getCurrentCell().row).toBe(0);
        expect(item.getCurrentCell().col).toBe(0);
    });

    it("get trail", () => {
        const item = new Item(3);
        item.addCell(new Cell(0, 0));
        item.addCell(new Cell(1, 0));
        item.addCell(null);
        item.addCell(new Cell(2, 0));
        expect(item.getTrail().length).toBe(3);
    });
});