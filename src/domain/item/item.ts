import { v4 as uuid } from 'uuid'
import { Cell } from '../cell/cell';

export class Item {

    protected id: string = uuid();
    protected trail: Cell[] = [];
    protected deleted = false;

    constructor(
        protected type: number, 
        protected level: number = 1) {}

    getId(): string {
        return this.id;
    }

    getLevel(): number {
        return this.level;
    }

    getType(): number{
        return this.type;
    }

    addCell(cell: Cell): void {
        if (cell){
            this.trail.push(cell);
        }        
    }

    getCurrentCell(): Cell {
        return (this.trail) ? this.trail[this.trail.length - 1] : null;
    }

    getTrail(): Cell[] {
        return this.trail;        
    }

    isDeleted(): boolean {
        return this.deleted;
    }

    destroy(): void {
        this.deleted = true;
    }

    clearTrail(): void {
        if (this.trail.length > 1){
            this.trail = this.trail.slice(-1);
        }        
    }
}