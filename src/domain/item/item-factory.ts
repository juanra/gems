import { Item } from "./item";

export class ItemFactory{

    min = 1;
    max = 5;

    constructor() {}

    makeItem(type: number, level = 1): Item{
        return new Item(type, level);
    }

    makeRandomItem(): Item{
        const type = Math.floor(Math.random() * (this.max - this.min + 1)) + 1;
        return new Item(type);
    }
}