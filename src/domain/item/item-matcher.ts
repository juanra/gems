import { Cell } from "../cell/cell";


export class ItemMatcher{
    constructor() {}

    public findMatches(cells: Cell[]): Cell[] {
        let current = 0;
        let matches: Cell[] = [];
        for (let i = 0; i < cells.length; i++) {
            const cell = cells[i];            
            if (!cell){
                continue;
            }
            const item = cell.getItem();
            if (!item){
                if (matches.length >= 3){
                    return matches;
                }
                current = 0;
                matches = [];
                continue;
            }

            if (current != item.getType()) {
                if (matches.length >= 3){
                    return matches;
                }
                current = item.getType();
                matches = [cell];
            }
            else {
                matches.push(cell);
            }
        }
        return (matches.length >= 3) ? matches: [];
    }
}