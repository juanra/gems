import { CellMatrix } from "../cell-matrix/cell-matrix";
import { Item } from "./item";
import { ItemSwapper } from "./item-swapper";

describe("ItemSwapper", () => {

    let swapper: ItemSwapper;
    let matrix: CellMatrix;

    beforeAll(()=> {
        swapper = new ItemSwapper();
    });

    beforeEach(()=> {
        matrix = new CellMatrix(9, 9);
    });

    it("swap items", ()=> {
        const a = new Item(1);
        const b = new Item(2);
        matrix.set(2,3, a);
        matrix.set(7,5, b);
        swapper.swap(matrix, 2, 3, 7, 5);
        expect(matrix.get(2,3)).toBe(b);
        expect(matrix.get(7,5)).toBe(a);
    });

    it("swap item with null", () => {
        const a = new Item(2);
        matrix.set(3, 5, a);
        swapper.swap(matrix, 3, 5, 8, 1);
        expect(matrix.get(3, 5)).toBeNull();
        expect(matrix.get(8, 1)).toBe(a);
    });

});