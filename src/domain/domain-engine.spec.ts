import { CellMatrix } from "./cell-matrix/cell-matrix";
import { CellMatrixFactory } from "./cell-matrix/cell-matrix-factory";
import { DomainEngine } from "./domain-engine";
import { Item } from "./item/item";
import { ItemFactory } from "./item/item-factory";

describe("DomainEngine", ()=> {

    let engine: DomainEngine;
    let factory: CellMatrixFactory;

    beforeAll(() => {
        engine = new DomainEngine();
        factory = new CellMatrixFactory(new ItemFactory());
    });

    beforeEach(() => {
        
    });

    it("set matrix", ()=>{
        const matrix = new CellMatrix(2,2);
        engine.setMatrix(matrix);
        expect(engine.getMatrix()).toBe(matrix);
    });

    it("match row", () => {
        const matrix = factory.makeEqualRows([1, 2, 2, 2, 3, 3, 3, 1, 2]);
        engine.setMatrix(matrix);
        const matches = engine.match();
        expect(matches.length).toBe(3);
        expect(matches[0].getItem().getType()).toBe(2);
    });

    it("match col", () => {
        const matrix = new CellMatrix(3, 3);
        matrix.set(0, 0, new Item(4));
        matrix.set(1, 0, new Item(4));
        matrix.set(2, 0, new Item(4));
        matrix.set(0, 1, new Item(1));
        matrix.set(1, 1, new Item(2));
        matrix.set(2, 2, new Item(3));
        matrix.set(0, 2, new Item(1));
        matrix.set(1, 2, new Item(2));
        matrix.set(2, 2, new Item(3));
        engine.setMatrix(matrix);
        const matches = engine.match();
        expect(matches.length).toBe(3);
        expect(matches[0].getItem().getType()).toBe(4);
    });

    it("destroy matches", () => {
        const matrix = factory.makeEqualRows([1, 2, 2, 2, 3, 3, 3, 1, 2]);
        engine.setMatrix(matrix)
        const matches = engine.match();
        engine.destroy(matches);
        const row = matrix.getRow(0);
        expect(row[1].getItem()).toBeNull();
        expect(row[2].getItem()).toBeNull();
        expect(row[3].getItem()).toBeNull();
    });

    it("refill row", () => {
        const matrix = factory.makeEqualRows([1, 2, 2, 2, 3, 3, 3, 1, 2]);
        engine.setMatrix(matrix);
        const matches = engine.match();
        engine.destroy(matches);
        engine.refill();
        const row = matrix.getRow(0);
        expect(row[1].getItem().getType()).toBe(2);
        expect(row[2].getItem().getType()).toBe(2);
        expect(row[3].getItem().getType()).toBe(2);
    });

    it("refill last row", () => {
        const matrix = factory.makeEqualRows([1, 2, 2, 2, 3, 3, 3, 1, 2]);
        engine.setMatrix(matrix);
        const matches = matrix.getRow(8).slice(1, 4);       
        engine.destroy(matches);
        engine.refill();
        const row = matrix.getRow(8);
        expect(row[1].getItem()).not.toBeNull();
        expect(row[2].getItem()).not.toBeNull();
        expect(row[3].getItem()).not.toBeNull();
    });

    it("refill all rows", () => {
        const matrix = factory.makeEqualRows([1, 2, 2, 2, 3, 3, 3, 1, 2]);
        engine.setMatrix(matrix);
        const matches = engine.match();
        engine.destroy(matches);
        engine.refill();
        const empty = matrix.getEmptyCells();
        expect(empty.length).toBe(0);
    });

    it("refill col", () => {
        const matrix = new CellMatrix(3, 3);
        matrix.set(0, 0, null);
        matrix.set(1, 0, null);
        matrix.set(2, 0, null);
        matrix.set(0, 1, new Item(1));
        matrix.set(1, 1, new Item(2));
        matrix.set(2, 2, new Item(3));
        matrix.set(0, 2, new Item(1));
        matrix.set(1, 2, new Item(2));
        matrix.set(2, 2, new Item(3));
        engine.setMatrix(matrix);
        engine.refill();
        const empty = matrix.getEmptyCells();
        expect(empty.length).toBe(0);
        expect(matrix.get(0,0).getTrail().length).toBe(3);
        expect(matrix.get(1,0).getTrail().length).toBe(2);
        expect(matrix.get(2,0).getTrail().length).toBe(1);
    })
});