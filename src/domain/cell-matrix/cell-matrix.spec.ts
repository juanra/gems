import { Item } from "../item/item";
import { CellMatrix } from "./cell-matrix";


describe("CellMatrix", () => {
    let matrix: CellMatrix;

    beforeEach(() => {
        matrix = new CellMatrix(9, 9);
    });

    it("matrix rows", () => {
        expect(matrix.rows).toBe(9);
    });

    it("matrix cols", () => {
        expect(matrix.cols).toBe(9);
    });

    it("set cell value", () => {
        const item = new Item(3);
        matrix.set(2, 4, item);
        expect(matrix.get(2, 4)).toBe(item);
    });
    
    it("destro cell value", () => {
        const item = new Item(3);
        matrix.set(2, 4, item);
        matrix.destroy(2, 4);
        expect(matrix.get(2, 4)).toBeNull();
    });

    it("get row cells", () => {
        const cells = matrix.getRow(0);
        expect(cells.length).toBe(9);
        expect(cells[3].row).toBe(0);
        expect(cells[3].col).toBe(3);
    });

    it("get col cells", () => {
        const cells = matrix.getCol(0);
        expect(cells.length).toBe(9);
        expect(cells[3].row).toBe(3);
        expect(cells[3].col).toBe(0);
    });

    it("destroy row", () => {
        const cells = matrix.getRow(3);
        matrix.destroyCells(cells);
        const after = matrix.getRow(3);
        after.map((cell) => {
            expect(cell.getItem()).toBeNull();
        });
    });

    it("destroy col", () => {
        const cells = matrix.getCol(3);
        matrix.destroyCells(cells);
        const after = matrix.getCol(3);
        after.map((cell) => {
            expect(cell.getItem()).toBeNull();
        });
    });

    it("set item cell to trail", () => {
        const item = new Item(3);
        matrix.set(0, 0, item);
        expect(item.getCurrentCell()).not.toBeNull();
        expect(item.getCurrentCell().row).toBe(0);
        expect(item.getCurrentCell().col).toBe(0);
    });

    it("get all empty cells", () => {
        const cells = matrix.getEmptyCells();
        expect(cells.length).toBe(9*9);
    })

    it("get empty cells", () => {
        matrix.set(0,0, new Item(3));
        const cells = matrix.getEmptyCells();
        expect(cells.length).toBe(9*9 - 1);
    })

    it("get all items", () => {
        matrix.set(0, 0, new Item(3));
        matrix.set(1, 2, new Item(2));
        const items = matrix.getItems();
        expect(items.length).toBe(2);
    });
});