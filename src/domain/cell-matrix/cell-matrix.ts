import { Cell } from "../cell/cell";
import { Item } from "../item/item";


export class CellMatrix {

    protected matrix: Cell[][];
    
    constructor(
        public readonly rows: number, 
        public readonly cols: number) {
        this.matrix = Array.from({length: rows}, (v, row) => Array.from({length: cols}, (v, col) => new Cell(row, col)));
    }

    isValidCell(row: number, col: number): boolean {
        return (row >= 0 && row < this.rows && col >= 0 && col < this.cols);
    }

    get(row: number, col: number): Item {
        const cell = this.matrix[row][col];
        return (cell) ? cell.getItem() : null;
    }

    set(row: number, col: number, item: Item): void {
        if (item) {
            item.addCell(this.matrix[row][col]);
        }        
        this.matrix[row][col].setItem(item);
    }

    getRow(row: number): Cell[] {
        return this.matrix[row];
    }

    getCol(col: number): Cell[] {
        const cells: Cell[] = [];
        for (let i = 0; i < this.rows; i++){
            cells.push(this.matrix[i][col]);
        }
        return cells;
    }

    destroy(row: number, col: number): void {
        const item = this.get(row, col);
        if (item){
            item.destroy();
        }
        this.set(row, col, null);
    }

    destroyCells(cells: Cell[]): void {
        cells.map((cell) => this.destroy(cell.row, cell.col));
    }

    getEmptyCells(): Cell[] {
        const empty: Cell[] = [];
        for (let i = 0; i < this.rows; i++)
        {
            for (let j = 0; j < this.cols; j++){
                if (this.get(i, j) == null){
                    empty.push(this.matrix[i][j]);
                }
            }        
        } 
        return empty;
    }

    getItems(): Item[] {
        const items: Item[] = [];
        for (let i = 0; i < this.rows; i++)
        {
            for (let j = 0; j < this.cols; j++){
                const item = this.get(i, j);
                if (item){
                    items.push(item);
                }
            }        
        } 
        return items;
    }

    swap(a: Item, b:Item): void {
        const cellA = a.getCurrentCell();
        const cellB = b.getCurrentCell();
        this.set(cellA.row, cellA.col, b);
        this.set(cellB.row, cellB.col, a);
    }

    print(): void {
        const rows = [];
        for (let i = 0; i < this.rows; i++)
        {
            const cols =  [];
            for (let j = 0; j < this.cols; j++){
                const item = this.get(i, j);
                cols.push((item) ? item.getType() : 0);
            }   
            rows.push(cols);         
        }        
        console.log(rows);
    }
}