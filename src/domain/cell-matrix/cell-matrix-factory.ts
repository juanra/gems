import { ItemFactory } from "../item/item-factory";
import { CellMatrix } from "./cell-matrix";


export class CellMatrixFactory {
    constructor(
        protected itemFactory: ItemFactory) {}

    makeRandomized(rows: number, cols: number): CellMatrix {
        const matrix = new CellMatrix(rows, cols);
        for (let i = 0; i < rows; i++){
            for (let j = 0; j < cols; j++) { 
                const item = this.itemFactory.makeRandomItem();
                matrix.set(i, j, item);
            }
        }
        return matrix;
    }

    makeEqualRows(rows: number[]): CellMatrix {
        const matrix = new CellMatrix(rows.length, rows.length);
        for (let i = 0; i < rows.length; i++) {
            for (let j = 0; j < rows.length; j++){
                const item = this.itemFactory.makeItem(rows[j]);
                matrix.set(i, j, item);
            }
        }
        return matrix;
    }

    makeEqualCols(cols: number[]): CellMatrix {
        const matrix = new CellMatrix(cols.length, cols.length);
        for (let i = 0; i < cols.length; i++) {
            for (let j = 0; j < cols.length; j++){
                const item = this.itemFactory.makeItem(cols[j]);
                matrix.set(j, i, item);
            }
        }
        return matrix;
    }
}