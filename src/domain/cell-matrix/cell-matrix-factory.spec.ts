import { ItemFactory } from "../item/item-factory";
import { CellMatrix } from "./cell-matrix";
import { CellMatrixFactory } from "./cell-matrix-factory";


describe("CellMatrixFactory", () => {
    let factory: CellMatrixFactory;

    beforeAll(() => {
        const itemFactory = new ItemFactory();
        factory = new CellMatrixFactory(itemFactory);
    })

    it("make randomized matrix", () => {
        const matrix: CellMatrix = factory.makeRandomized(4, 4);
        expect(matrix.rows).toBe(4);
        expect(matrix.cols).toBe(4);
        for (let i = 0; i < matrix.rows; i++){
            for (let j = 0; j < matrix.cols; j++){
                const item = matrix.get(i, j);
                expect(item.getType()).toBeGreaterThan(0);
            }
        }
    });

    it("make equal rows matrix", () => {
        const row: number[] = [1, 2, 2, 2, 3, 3, 3, 1, 2];
        const matrix: CellMatrix = factory.makeEqualRows(row);
        expect(matrix.get(0, 2).getType()).toBe(matrix.get(1, 2).getType());
    });

    it("make equal cols matrix", () => {
        const col: number[] = [1, 2, 2, 2, 3, 3, 3, 1, 2];
        const matrix: CellMatrix = factory.makeEqualCols(col);
        expect(matrix.get(0, 2).getType()).toBe(matrix.get(0, 2).getType());
    });
});