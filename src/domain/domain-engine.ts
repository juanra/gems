import { CellMatrix } from "./cell-matrix/cell-matrix";
import { CellMatrixFactory } from "./cell-matrix/cell-matrix-factory";
import { Cell } from "./cell/cell";
import { Item } from "./item/item";
import { ItemFactory } from "./item/item-factory";
import { ItemMatcher } from "./item/item-matcher";
import { ItemSwapper } from "./item/item-swapper";
import { Score } from "./score/score";


export class DomainEngine {

    protected itemFactory: ItemFactory;
    protected matcher: ItemMatcher;
    protected swapper: ItemSwapper;
    protected matrixFactory: CellMatrixFactory;
    protected matrix: CellMatrix;
    protected points: Score;
    protected moves: Score;

    constructor() {
        this.itemFactory = new ItemFactory();
        this.matcher = new ItemMatcher();
        this.swapper = new ItemSwapper();
        this.matrixFactory = new CellMatrixFactory(this.itemFactory);
    }

    init(): void {        
        this.points = new Score();
        this.moves = new Score();
        this.matrix = this.matrixFactory.makeRandomized(9, 9);
        
        // valid staring matrix
        let cells = this.match();
        while(cells.length > 0){
            this.destroy(cells);
            this.refill();
            cells = this.match();
        }
        this.matrix.getItems().map((i) => i.clearTrail());
    }

    setMatrix(matrix: CellMatrix): void {
        this.matrix = matrix;
    }

    getMatrix(): CellMatrix {
        return this.matrix;
    }

    getPoints(): Score {
        return this.points;
    }

    getMoves(): Score {
        return this.moves;
    }
    
    match(target: Item = null): Cell[] {
        const all = [];
        for (let i = 0; i < this.matrix.rows; i++){
            const row = this.matrix.getRow(i);
            const matches = this.matcher.findMatches(row);
            if (matches.length > 0){
                all.push(matches);
            }
        }
        for (let j = 0; j < this.matrix.cols; j++){
            const col = this.matrix.getCol(j);
            const matches = this.matcher.findMatches(col);
            if (matches.length > 0){
                all.push(matches);
            }
        }

        let index = 0;
        if (all.length == 0){
            return [];
        }
        else if (target){
            all.map((matches, i) => {
                const found = matches.find((cell) => cell.getItem().getId() == target.getId());
                if (found){
                    index = i;
                }
            });
        }
        return all[index];
    }

    destroy(cells: Cell[]): void {
        this.matrix.destroyCells(cells);
    }

    refill(): void {
        let cells = this.matrix.getEmptyCells();
        while(cells.length > 0) {
            this.refillCells(cells);
            cells = this.matrix.getEmptyCells();
        }        
    }

    refillCells(cells: Cell[]): void {
        cells.map((cell) => {
            if (cell.row + 1 == this.matrix.rows) {
                const item = this.itemFactory.makeRandomItem();
                this.matrix.set(cell.row, cell.col, item);                
            }
            else if (this.matrix.get(cell.row + 1, cell.col)) {
                this.swapper.swap(this.matrix, cell.row, cell.col, cell.row + 1, cell.col);               
            }            
        });
    }

    swap(a: Item, b: Item): void {
        this.matrix.swap(a, b);
    }

    simulateSwap(a: Item, b: Item): boolean {
        this.matrix.swap(a, b);
        const matchesA = this.match(a);
        const matchesB = this.match(b);
        this.matrix.swap(b, a);
        a.clearTrail();
        b.clearTrail();
        return matchesA.length > 0 || matchesB.length > 0;
    }
}