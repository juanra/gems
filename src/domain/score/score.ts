export class Score {

    protected value = 0;

    get(): number {
        return this.value;
    }

    add(value: number): void {
        this.value += value;
    }

    reset(): void {
        this.value = 0;
    }
}