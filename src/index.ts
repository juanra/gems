import { Clock } from 'three';
import { DomainEngine } from './domain/domain-engine';
import { GameEngine } from './game/game-engine';
import { InputEngine } from './input/input-engine';
import { RenderEngine } from './render/render-engine';


const domain = new DomainEngine();
const render = new RenderEngine(domain);
const input = new InputEngine(domain, render);

const engine = new GameEngine(domain, render, input);

const clock = new Clock();
const loop = function () {
    const dt = clock.getDelta();
    engine.update(dt);
    requestAnimationFrame(loop);
};
loop();