import { Item } from "../../domain/item/item";
import { InputEvent } from "./input-event";

export class SwapInputEvent extends InputEvent{
    constructor(protected a: Item, protected b: Item) {        
        super();
    }

    getA(): Item {
        return this.a;
    }

    getB(): Item {
        return this.b;
    }
}