import { Vector2 } from "three";
import { DomainEngine } from "../domain/domain-engine";
import { Item } from "../domain/item/item";
import { ItemRender } from "../render/item/item-render";
import { RenderEngine } from "../render/render-engine";
import { SwapInputEvent } from "./events/swap-input-event";
import { InputEvent} from "./events/input-event";

export class InputEngine {

    protected timer = 0;
    protected actions: InputEvent[] = [];

    protected mouse: Vector2 = new Vector2();
    protected start: Vector2 = null;
    protected end: Vector2 = null;
    protected direction: string = null;
    protected pick: ItemRender = null;

    constructor(
        protected domain: DomainEngine, 
        protected render: RenderEngine) {}

    updateMouse(event: any): void {
        this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        this.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    }

    reset(): void {
        this.pick = null;
        this.direction = null;
        this.start = null;
        this.end = null;
    }

    init(): void {
        const dom = this.render.getRenderer().domElement;

        dom.addEventListener('pointerdown', (event) => {
            this.reset();

            this.updateMouse(event);

            this.pick = this.render.pick(this.mouse);            
            this.start = this.mouse.clone();            
        }, false);

        dom.addEventListener('pointerup', (event) => {
            if (!this.end || !this.start){
                this.reset();
                return;
            }
            
            const direction = this.end.sub(this.start);
            if(Math.abs(direction.x) > Math.abs(direction.y)){
                this.direction = (direction.x > 0) ? 'right': 'left';
            }
            else{
                this.direction = (direction.y > 0) ? 'up' : 'down';
            }
        }, false);

        dom.addEventListener('pointermove', (event) => {
            this.updateMouse(event);
            this.end = this.mouse.clone();
        }, false);
    }

    getActions(): InputEvent[] {
        return this.actions;
    }

    clearActions(): void {
        this.actions = [];
    }

    update(dt: number): void {
        if (!this.pick || !this.direction){
            return;
        }

        let rowA = this.pick.getItem().getCurrentCell().row;
        let colA = this.pick.getItem().getCurrentCell().col;
        let rowB = rowA;
        let colB = colA;
        switch(this.direction){
            case 'left': colB--; break;
            case 'right': colB++; break;
            case 'up': rowB++; break;
            case 'down': rowB--; break;
        }

        const matrix = this.domain.getMatrix();
        if (!matrix.isValidCell(rowB, colB))
        {
            this.reset();
            return;
        }

        const pick = this.pick.getItem();
        const target = matrix.get(rowB, colB);
        if (!this.isValidSwap(pick, target)){
            this.reset();
            return;
        }  

        this.actions.push(new SwapInputEvent(pick, target));   
        this.reset();     
    }

    protected isValidSwap(pick: Item, target: Item): boolean {
        if (!target){
            return false;
        }
        if (pick.getType() == target.getType()){
            return false;
        }
        
        if (!this.domain.simulateSwap(pick, target)) {
            return false;
        }

        return true;
    }
}