import { GameEngine } from "../game-engine";
import { InputGameState } from "./input-game-state";
import { GameState } from "./game-state";
import { SpawnItemRenderAnimation } from "../../render/item/spawn-item-render-animation";

enum LoadGameStateEnum {
    Update,
    Animate
}

export class LoadGameState extends GameState{

    protected state: LoadGameStateEnum;

    constructor(engine: GameEngine){
        super(engine);
        this.state = LoadGameStateEnum.Update;
    }
    
    update(dt: number): void {
        super.update(dt);

        if (this.state == LoadGameStateEnum.Update) {
            const matrix = this.domain.getMatrix();
            const collection = this.render.getCollection();
            matrix.getItems().map((item) => {
                let itemRender = collection.find(item.getId());
                if (!itemRender) {
                    itemRender = this.render.getItemRenderFactory().makeItemRender(item);
                    itemRender.addAnimation(new SpawnItemRenderAnimation(itemRender, 0));       
                    collection.add(itemRender);                    
                }
            });
            this.state = LoadGameStateEnum.Animate;
        }
        else if (this.state == LoadGameStateEnum.Animate){
            if (this.wait(0.5)){
                return;
            }
            this.setNextState(new InputGameState(this.engine));
        }  
    }
}