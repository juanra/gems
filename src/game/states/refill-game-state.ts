import { FallItemRenderAnimation } from "../../render/item/fall-item-render-animation";
import { ItemRender } from "../../render/item/item-render";
import { SpawnItemRenderAnimation } from "../../render/item/spawn-item-render-animation";
import { GameEngine } from "../game-engine";
import { GameState } from "./game-state";
import { MatchGameState } from "./match-game-state";

enum RefillGameStateEnum {
    Update,
    Animate
}

export class RefillGameState extends GameState{

    protected state: RefillGameStateEnum;
    protected delay = 0;
    protected delayStepsTime = 0.02;

    constructor(engine: GameEngine){
        super(engine);
        this.state = RefillGameStateEnum.Update;
    }
    
    update(dt: number): void {
        super.update(dt);

        if (this.state == RefillGameStateEnum.Update) {

            this.domain.refill();

            this.createAnimationChain(
                this.sortItemsForAnimations(
                    this.sync()));

            this.state = RefillGameStateEnum.Animate;
        }
        else if (this.state == RefillGameStateEnum.Animate){
            if (this.wait(this.delay+ 0.5)){
                return;
            }
            this.setNextState(new MatchGameState(this.engine));
        }  
    }

    /**
     * Track new items and returns updated & new items.
     * 
     * @returns ItemRender[]
     */
    protected sync(): ItemRender[] {
        const matrix = this.domain.getMatrix();
        const collection = this.render.getCollection();
        const updated: ItemRender[] = [];
        matrix.getItems().map((item) => {
            let itemRender = collection.find(item.getId());
            if (!itemRender) {
                itemRender = this.render.getItemRenderFactory().makeItemRender(item);
                collection.add(itemRender); 
                updated.push(itemRender);
            }
            else {
                const trail = item.getTrail(); 
                if (trail.length > 1) {
                    updated.push(itemRender);
                }
            }
        });
        return updated;
    }

    /**
     * Create chain of fall & spawn aniations
     */
    protected createAnimationChain(items: ItemRender[]): void {
        items.map((itemRender: ItemRender) => {
            const trail = itemRender.getItem().getTrail();
            if(itemRender.isNew()){
                itemRender.addAnimation(new SpawnItemRenderAnimation(itemRender, this.delay)); 
                this.delay += this.delayStepsTime;
            }
            itemRender.addAnimation(new FallItemRenderAnimation(itemRender, this.delay, this.delayStepsTime));
            this.delay += this.delayStepsTime * trail.length;
        });
    }

    /**
     * Sort items by priority to be animated:
     * - send new to the end of the list
     * - longer trails first
     * - lower row first
     * - left rows first
     */
    protected sortItemsForAnimations(items: ItemRender[]): ItemRender[] {
        return items.sort((a: ItemRender, b: ItemRender) => {  

            // send new to the end of the list
            if (a.isNew() && !b.isNew()){
                return 1;
            } else if (!a.isNew() && b.isNew()){
                return -1;
            }

            const itemA = a.getItem();
            const itemB = b.getItem();
            const trailA = itemA.getTrail().length;
            const trailB = itemB.getTrail().length;
            
            if (trailA == trailB && trailA > 0) {
                const cellA = itemA.getCurrentCell();
                const cellB = itemB.getCurrentCell();
                
                if (cellA.row == cellB.row){

                    // left rows first
                    return (cellA.col < cellB.col) ? -1 : 1;
                }

                // lower row first
                return (cellA.row < cellB.row) ? -1 : 1;
            }

            // longer movement first
            return (trailA > trailB) ? -1 : 1;
        });
    }
}