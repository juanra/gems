import { DomainEngine } from "../../domain/domain-engine";
import { RenderEngine } from "../../render/render-engine";
import { GameEngine } from "../game-engine";

export abstract class GameState {

    protected done = false;
    protected timer = 0;
    protected nextState: GameState;
    protected domain: DomainEngine;
    protected render: RenderEngine;
    protected animations: Animation[] = [];

    protected constructor(
        protected engine: GameEngine){
            this.domain = engine.getDomain();
            this.render = engine.getRender();
        }

    isDone(): boolean {
        return this.done;
    }

    update(dt: number): void {
        this.timer += dt;
    }

    next(): GameState {
        return this.nextState;
    };

    protected setNextState(state: GameState): void {
        this.nextState = state;
        this.done = true;
    }

    wait(seconds: number): boolean {
        return this.timer < seconds;
    }

    resetTimer(): void {
        this.timer = 0;
    }

    protected addAnimation(animation: Animation): void {
        this.animations.push(animation);
    }

    // protected animate(dt: number): boolean {
    //     let done = true;
    //     this.animations.map((animation) => {
    //         animation.update(dt);
    //         done = done && animation.isDone();
    //     });
    //     return done;
    // }
}