import { SwapInputEvent } from "../../input/events/swap-input-event";
import { InputEngine } from "../../input/input-engine";
import { GameEngine } from "../game-engine";
import { GameState } from "./game-state";
import { SwapGameState } from "./game-swap-state";

export class InputGameState extends GameState {

    protected input: InputEngine;

    constructor(engine: GameEngine){
        super(engine);
        this.input = engine.getInput();
        this.input.reset();
    }

    update(dt: number): void {
        super.update(dt);

        this.input.update(dt);

        const actions = this.input.getActions();        
        actions.map((action) => {
            if (action instanceof SwapInputEvent){
                this.setNextState(new SwapGameState(this.engine, action));
            }
        });
        this.input.clearActions();
    }
}