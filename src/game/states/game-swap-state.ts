import { Item } from "../../domain/item/item";
import { SwapInputEvent } from "../../input/events/swap-input-event";
import { FallItemRenderAnimation } from "../../render/item/fall-item-render-animation";
import { GameEngine } from "../game-engine";
import { GameState } from "./game-state";
import { MatchGameState } from "./match-game-state";


enum SwapGameStateEnum{
    Update,
    Animate
}

export class SwapGameState extends GameState {

    protected state: SwapGameStateEnum = SwapGameStateEnum.Update;
    protected pick: Item = null;
    protected target: Item = null;

    constructor(engine: GameEngine, protected action: SwapInputEvent){
        super(engine);
    }
    
    update(dt: number): void {
        super.update(dt);

        switch(this.state){
            case SwapGameStateEnum.Update:

                this.pick = this.action.getA();
                this.target = this.action.getB();

                this.domain.swap(this.pick, this.target);

                const collection = this.engine.getRender().getCollection();
                const itemRenderA = collection.find(this.pick.getId());
                const itemRenderB = collection.find(this.target.getId());

                itemRenderA.addAnimation(new FallItemRenderAnimation(itemRenderA, 0, 0));
                itemRenderB.addAnimation(new FallItemRenderAnimation(itemRenderB, 0, 0));

                this.domain.getMoves().add(1);

                this.state = SwapGameStateEnum.Animate;
            break;
            case SwapGameStateEnum.Animate:
                if(this.wait(0.5)){
                    return;
                }
                this.setNextState(new MatchGameState(this.engine, this.pick)); 
                break;
        }
        
              
    }
}