import { GameEngine } from "../game-engine";
import { RefillGameState } from "./refill-game-state";
import { GameState } from "./game-state";
import { Cell } from "../../domain/cell/cell";
import { Item } from "../../domain/item/item";
import { ItemFactory } from "../../domain/item/item-factory";
import { DestroyItemRenderAnimation } from "../../render/item/destroy-item-render-animation";
import { SpawnItemRenderAnimation } from "../../render/item/spawn-item-render-animation";

enum DestroyGameStateEnum {
    Update,
    Animate
}

export class DestroyGameState extends GameState {

    protected items: Item[];
    protected state: DestroyGameStateEnum;
    protected itemFactory: ItemFactory;

    protected delay = 0;
    protected delayStepsTime = 0.15;

    constructor(engine: GameEngine, protected matches: Cell[]){
        super(engine);
        this.itemFactory = new ItemFactory();
        this.items = this.matches.map((cell)=> (cell.getItem()));
        this.state = DestroyGameStateEnum.Update;
    }

    update(dt: number): void {
        super.update(dt);

        if (this.state == DestroyGameStateEnum.Update) {
            const cell = this.matches[0];

            this.domain.destroy(this.matches);

            this.updateScore();

            this.items.map((item) => {
                const renderItem = this.render.getCollection().find(item.getId());
                renderItem.addAnimation(new DestroyItemRenderAnimation(renderItem, this.delay));
                this.delay += this.delayStepsTime;
            });

            // booster
            if (this.items.length > 3){
                const item = this.items[0];
                this.addBooster(cell, item.getType(), item.getLevel() + 1);
            }

            this.state = DestroyGameStateEnum.Animate;
        }
        else if (this.state == DestroyGameStateEnum.Animate){
            if (this.wait(this.delay)){
                return;
            }
            this.setNextState(new RefillGameState(this.engine));
        }  
    }

    protected addBooster(cell: Cell, type: number, level: number): void {
        const booster = this.itemFactory.makeItem(type, level);
        this.domain.getMatrix().set(cell.row, cell.col, booster);
        const itemRender = this.render.getItemRenderFactory().makeItemRender(booster);
        this.render.getCollection().add(itemRender);
        itemRender.addAnimation(new SpawnItemRenderAnimation(itemRender, this.delay));
        
    }

    protected updateScore(): void {
        let points = 0;
        this.items.map((item) => {
            points += 50 * item.getLevel();
        });        
        this.domain.getPoints().add(points);
    }

    
}