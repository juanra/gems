import { DestroyGameState } from "./destroy-game-state";
import { GameEngine } from "../game-engine";
import { InputGameState } from "./input-game-state";
import { GameState } from "./game-state";
import { Item } from "../../domain/item/item";

export class MatchGameState extends GameState {

    constructor(engine: GameEngine, protected target: Item = null){
        super(engine);
    }
    
    update(dt: number): void {
        super.update(dt);
        
        const matches = this.domain.match(this.target);        
        if (matches.length > 0){
            this.setNextState(new DestroyGameState(this.engine, matches));
        }
        else{
            this.setNextState(new InputGameState(this.engine));
        }        
    }
}