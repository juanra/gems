import { DomainEngine } from "../domain/domain-engine";
import { InputEngine } from "../input/input-engine";
import { RenderEngine } from "../render/render-engine";
import { LoadGameState } from "./states/load-game-state";

export class GameEngine {  

    protected state: any;

    constructor(
        protected domain: DomainEngine,
        protected render: RenderEngine,
        protected input: InputEngine
    ) { 
        this.input.init();
        this.domain.init();
        this.render.init();
        this.state = new LoadGameState(this);
    }

    getInput(): InputEngine {
        return this.input;
    }

    getDomain(): DomainEngine {
        return this.domain;
    }

    getRender(): RenderEngine {
        return this.render;
    }
    
    update(dt: number): void {        
        if (!this.state) {
            return;
        }

        this.state.update(dt);
        this.render.update(dt);

        if (this.state.isDone()) {
            this.state = this.state.next();            
        }        
    }
}